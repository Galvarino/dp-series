# Question Link: https://leetcode.com/problems/coin-change/

class Solution:
    INT_MAX = 10**7

    def coin_change_rec(self, coins: List[int], amount: int) -> int:
        if amount == 0:
            return 0

        if amount < 0:
            return self.INT_MAX

        min_val = self.INT_MAX

        for coin in coins:
            min_val = min(1 + self.coin_change_rec(coins, amount - coin), min_val)

        return min_val

    def coin_change_mem(self, coins: List[int], amount: int, mem: List[int]) -> int:
        if amount == 0:
            return 0

        if amount < 0:
            return self.INT_MAX

        if mem[amount] == -1:
            min_val = self.INT_MAX
            for coin in coins:
                min_val = min(1 + self.coin_change_mem(coins, amount - coin, mem), min_val)
            mem[amount] = min_val

        return mem[amount]

    def coin_change_tab(self, coins: List[int], amount: int) -> int:
        mem = [self.INT_MAX] * (amount + 1)
        mem[0] = 0
        min_coins = 0

        for i in range(1, amount + 1):
            min_val = self.INT_MAX

            for coin in coins:
                if i - coin >= 0:
                    min_val = min(1 + mem[i - coin], min_val)

            mem[i] = min_val

        return mem[amount]

    def coin_change(self, coins: List[int], amount: int) -> int:
        # mem = [-1] * (amount + 1)
        res = self.coin_change_tab(coins, amount)
        if res == self.INT_MAX:
            return -1
        return res