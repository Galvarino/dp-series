# Question Link: https://leetcode.com/problems/min-cost-climbing-stairs/

class Solution:
    def min_cost_rec(self, cost: List[int], pos: int) -> int:
        if pos >= len(cost):
            return 0
        return cost[pos] + min(self.min_cost_rec(cost, pos + 1, mem), self.min_cost_rec(cost, pos + 2, mem))

    def min_cost_mem(self, cost: List[int], pos: int, mem: List[int]) -> int:
        if pos >= len(cost):
            return 0
        if mem[pos] == -1:
            mem[pos] = cost[pos] + min(self.min_cost_rec(cost, pos + 1, mem), self.min_cost_rec(cost, pos + 2, mem))
        return mem[pos]
        
    def min_cost_tab(self, cost: List[int]) -> int:
        mem = [-1] * len(cost)
        mem[len(cost) - 1] = cost[len(cost) - 1]
        mem[len(cost) - 2] = cost[len(cost) - 2]
        for i in range(len(cost) - 3, -1, -1):
            mem[i] = cost[i] + min(mem[i + 1], mem[i + 2])
        return min(mem[0], mem[1])

    def min_cost_so(self, cost: List[int]) -> int:
        last1 = cost[len(cost) - 2]
        last2 = cost[len(cost) - 1]
        for i in range(len(cost) - 3, -1, -1):
            temp = cost[i] + min(last1, last2)
            last2 = last1
            last1 = temp
        return min(last1, last2)

    def min_cost_climbing_stairs(self, cost: List[int]) -> int:
        # mem = [-1] * len(cost)
        return self.min_cost_so(cost)
        
        
