# Question Link: https://leetcode.com/problems/house-robber/

class Solution:
    def house_robbery_rec(self, nums: List[int], pos: int) -> int:
        if pos >= len(nums):
            return 0
        incl = self.house_robbery_rec(nums, pos + 2) + nums[pos]
        excl = self.house_robbery_rec(nums, pos + 1)
        return max(incl, excl)

    def house_robbery_mem(self, nums: List[int], pos: int, mem: List[int]) -> int:
        if pos >= len(nums):
            return 0
        if mem[pos] == -1:
            incl = self.house_robbery_mem(nums, pos + 2, mem) + nums[pos]
            excl = self.house_robbery_mem(nums, pos + 1, mem)
            mem[pos] = max(incl, excl)
        return mem[pos]

    def house_robbery_tab(self, nums: List[int]) -> int:
        mem = [-1] * len(nums)
        mem[-1] = nums[-1]
        if len(nums) > 1:
            mem[-2] = max(nums[-1], nums[-2])
        for i in range(len(nums) - 3, -1, -1):
            mem[i] = max(mem[i + 1], mem[i + 2] + nums[i])
        return mem[0]

    def house_robbery_so(self, nums: List[int]) -> int:
        last2 = nums[-1]
        last1 = nums[-1]
        if len(nums) > 1:
            last1 = max(nums[-1], nums[-2])
        for i in range(len(nums) - 3, -1, -1):
            res = max(last1, last2 + nums[i])
            last2 = last1
            last1 = res
        return last1

    def rob(self, nums: List[int]) -> int:
        # mem = [-1] * len(nums)
        return self.house_robbery_so(nums)
