# Question Link: https://www.codingninjas.com/studio/problems/maximum-sum-of-non-adjacent-elements_843261

class Solution:
    def max_non_adj_sum_rec(self, nums, pos):
        if pos < 0:
            return 0
            
        incl = self.max_non_adj_sum_rec(nums, pos - 2) + nums[pos]
        excl = self.max_non_adj_sum_rec(nums, pos - 1)
        return max(incl, excl)


    def max_non_adj_sum_mem(self, nums, pos, mem):
        if pos < 0:
            return 0

        if mem[pos] == -1:
            incl = self.max_non_adj_sum_mem(nums, pos - 2, mem) + nums[pos]
            excl = self.max_non_adj_sum_mem(nums, pos - 1, mem)
            mem[pos] = max(incl, excl)

        return mem[pos]


    def max_non_adj_sum_tab(self, nums):
        mem = [-1] * len(nums)
        mem[0] = nums[0]

        if len(nums) > 1:
            mem[1] = max(nums[0], nums[1])

        for i in range(2, len(nums)):
            incl = mem[i - 2] + nums[i]
            excl = mem[i - 1]
            mem[i] = max(incl, excl)

        return mem[len(nums) - 1]


    def max_non_adj_sum_so(self, nums):
        last1 = last2 = nums[0]

        if len(nums) > 1:
            last1 = max(nums[0], nums[1])
    
        for i in range(2, len(nums)):
            incl = last2 + nums[i]
            excl = last1
            last2 = last1
            last1 = max(incl, excl)

        return last1


    def max_non_adj_sum(self, nums):    
        # mem = [-1] * len(nums)
        return self.max_non_adj_sum_so(nums)